var SocketServer = require('./SocketServer')

const Room = require('./Room')

var Users = {};
var Rooms = {};
var roomId = 0;
var WSArray = {};

var testUsers = {
    "0":{
        id:0,
        nickname:"用户0",
        headImgUrl:"https://noobres.mcfifa.cn/flows/0.png",
        sex:1
    },
    "1":{
        id:1,
        nickname:"用户1",
        headImgUrl:"https://noobres.mcfifa.cn/flows/0.png",
        sex:2
    },
    "2":{
        id:2,
        nickname:"用户2",
        headImgUrl:"https://noobres.mcfifa.cn/flows/0.png",
        sex:1
    },
    "3":{
        id:3,
        nickname:"用户3",
        headImgUrl:"https://noobres.mcfifa.cn/flows/0.png",
        sex:2
    }
}

class GameMain{
    constructor() {
        var Server = new SocketServer({port : 8001});
        this.server = Server;
        var self = this
        Server.addEvent('login', async function (ws, data) {
            console.log('login:', data);
            let user = testUsers[data.id];//这里直接写登陆，本工程假设直接登陆成功
            if (!user) {
                throw new Error('登陆失败，账号或密码不对')
            }
            // ws.bind(user.id);
            ws._userid = user.id
            WSArray[user.id] = ws;
            Users[user.id] = {
                id:user.id,
                nickname : user.nickname,
                headImgUrl : user.headImgUrl,
                sex : user.sex
            }
            Server.send(ws, {
                e: 'loginSuccess', 'user': {
                    nickname: user.nickname,
                    id: user.id,
                    sex: user.sex,
                    headImgUrl: user.headImgUrl
                }
            });
        })

        Server.addEvent('close', function (ws,data) {
            var user = Users[ws._userid]
            if(user && typeof(user.roomId) != 'undefined'){
                var room = Rooms[user.roomId]
                room && room.onUserExit(user);
            }
            delete Users[ws.id];
        })

        Server.addEvent('join', function (ws, data) {
            console.log('加入房间:', data);
            var room = Rooms[data.v]
            if (!room) {
                room = new Room(self, data.v);
                Rooms[data.v] = room;
            }
            var user = Users[ws._userid];
            user.roomId = room.id;
            room.onUserEnter(user);
        })

        Server.addEvent('diss',function(ws,data){
            // console.log('gamemessage:',data);
            var user = Users[ws._userid];
            var room = Rooms[user.roomId]
            if(room){
                room.broadcast('diss',{});
                self.destroyRoom(room);
                // throw new Error('未找到房间')
            }
            delete Users[ws.id]
        })

        Server.addEvent('game',function(ws,data){
            // console.log('gamemessage:',data);
            var user = Users[ws._userid];
            var room = Rooms[user.roomId]
            if(!room){
                throw new Error('未找到房间')
            }
            room.message(user,data.v);
        })

        this.frameTime = Date.now();
        setInterval(this.frameUpdate.bind(this),200);
    }
    frameUpdate(){
        var t = Date.now();
        for(var id in Rooms){
            Rooms[id].frameUpdate(t-this.frameTime);
        }
        this.frameTime = t;
    }

    destroyRoom(room){
        typeof(room.id) != 'undefined' && delete Rooms[room.id]
    }

    send(ws,data){
        try{
            if(typeof(ws) == 'object'){
                this.server.send(ws,data)
            } else if(typeof(ws) == 'number'){
                this.server.send(WSArray[Users[ws].id],data);
            }
        }catch (e) {
            console.log('send error',e.message)
        }

    }

}

new GameMain()
