
cc.Class({
    extends: require('BaseBullet'),

    properties: {
        attack : 0
    },

    setOptions(options){
        this.attack = options.attack;
    },
    getAttack(){
        return this.attack;
    },

    onColliderEnter(no){
        if(!this.node){
            return false;
        }
        no.onAttack(this);
        this.node.sprite ? this.node.sprite.recovery() : this.node.destroy();
        return true;
        // this._destroyImmediate();
    }
});
