var Enum = require('Enum')

cc.Class({
    extends: cc.Component,

    properties: {
        camp : {
            default :Enum.CAMP.neutral,
            type : Enum.CAMP,
            tooltip : 'neutral中立\nfriend友方\nenemy敌方'
        },
        rect:{
            type : cc.Rect,
            default : null
        },
        activeColldier:{
            set(v){
                if(v != this._activeColldier){
                    if(v){
                        global.GameLoop.GameMgr && global.GameLoop.GameMgr.colliderMgr.addUnit(this);
                    } else {
                        global.GameLoop.GameMgr && global.GameLoop.GameMgr.colliderMgr.removeUnit(this);
                    }
                    this._activeColldier = v;
                }
            }
        },
        _activeColldier:false
    },
    setType(t){
        if(t == undefined){
            cc.warn('无效的类型')
            return;
        }
        this.type =t ;
    },

    onDestroy(){
        this.activeColldier = false;
    },

    setCamp(camp){
        if(camp == undefined){
            cc.warn('无效的阵容')
            return;
        }
        this.camp = camp;
    }
});
