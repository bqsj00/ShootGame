var Enum = {
    RoleState : {
        Idle : 0,
        Run : 1
    },
    CAMP : cc.Enum({
        neutral : 0, //中立
        friend : 1, //友方
        enemy : 2, //敌方 
        wall:3     //不可催毁的
    }),
}

module.exports = Enum;